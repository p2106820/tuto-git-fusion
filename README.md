#                                                      TP noté Git - Durée : 3 heures

Instructions :
* Nous allons utiliser le dépôt disponible au lien suivant :

[https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git](https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git)   

* Faites une copie de ce dépôt dans votre espace personnel (un "fork"), puis cloner ce dépôt localement.

* Suivez les instructions, et réalisez les actions dans l'ordre indiqué.

* Vous avez le droit d'utiliser les documents de cours ainsi que les pages de manuel de Git.

* Toutes les commandes Git utilisées seront données dans le compte-rendu (ou insérées dans le README.md), et effectuées par la suite.

* La note finale tiendra compte de vos réponses, mais aussi de la qualité de vos messages de commit.


## I. Prise en main du dépôt (30 minutes – 4 points)

On travaillera dans un dépôt local.

1. Sur quelle branche êtes-vous ? 

   ```bash
   git branch 
   * main 
   ```

   i'm on main branch

   

2. Combien de branches contient ce dépôt ?

   ```bash
   git branch -a 
   * main
     remotes/origin/HEAD -> origin/main
     remotes/origin/develop
     remotes/origin/feature
     remotes/origin/main
   ```

   There are 3 branches

   

3. Pourquoi certaines branches apparaissent-elles en rouge ?

   Because they are remote branches

   

4. À quoi sert le fichier `.gitignore` ? Que contient-il ?

   .gitignore is a file where we store the name of files that we will not manage with git. In this repository, .gitignore contain example & example.o

   

5. Créez une nouvelle branche `test` à partir de la branche `main`.

   ```bash
   git branch test
   
   $ git branch
   * main
     test
   ```

   we use the following command to switch to test branch : 

   ```bash
   git switch test
   ```

   

6. Ajoutez une fonction `foobar()` au code C qui renvoie un entier. Elle ressemblera à ça :
```
int foobar() {
	return 1;
}
```
Et on ajoutera cela dans la fonction main() :
```    
int test = foobar() ;
printf("foobar returns :%d\n", test);
```
La compilation du code se fera comme suit, en utilisant le fichier `Makefile` :

    $ make

Ou directement en utilisant la commande :

    $ gcc -o example example.c

7. Faites un commit avec vos modifications.

   ```bash
   git add --all
   TP GIT NOTER/tuto-git-fusion (test)
   $ git status
   On branch test
   Changes to be committed:
     (use "git restore --staged <file>..." to unstage)
           modified:   README.md
           modified:   example.c
           new file:   example.exe
   ```

   ```bash
   $ git commit -m "foobar() function & first compilation"
   [test d19d090] foobar() function & first compilation
    3 files changed, 50 insertions(+), 3 deletions(-)
    create mode 100644 example.exe
   ```

8. Poussez votre branche sur le dépôt distant.

   Before to push the commit, we need to add test branch on the remote repository. 

   ```bash
   $ git push --set-upstream origin test
   Enumerating objects: 8, done.
   Counting objects: 100% (8/8), done.
   Delta compression using up to 8 threads
   Compressing objects: 100% (5/5), done.
   Writing objects: 100% (5/5), 17.19 KiB | 8.59 MiB/s, done.
   Total 5 (delta 1), reused 0 (delta 0), pack-reused 0
   remote:
   remote: To create a merge request for test, visit:
   remote:   https://forge.univ-lyon1.fr/p2106820/tuto-git-fusion/-/merge_requests/new?merge_request%5Bsource_branch%5D=test
   remote:
   To https://forge.univ-lyon1.fr/p2106820/tuto-git-fusion.git
    * [new branch]      test -> test
   Branch 'test' set up to track remote branch 'test' from 'origin'.
   ```

   Now, we can see on gitlab web interface, my test branch with my files updated 

   ![image-20230302145552385](C:\Users\marti\AppData\Roaming\Typora\typora-user-images\image-20230302145552385.png)

## II. Fusion de branches avec conflit (30 minutes – 4 points)

1. Revenez sur la branche `main`.

   ```bash
   git switch main
   ```

2. Les deux stratégies principales de fusion de branches sont appelées par avance rapide, ou encore "fast-forward" (anglais), et récursive ou "non-fast-forward" (anglais). Quelle est la principale différence entre ces deux stratégies de fusion (qui représentent les deux principalement utilisées).

   The `--no-ff` option is useful when you want to have a clear notion of your feature branch. So even if in the meantime no commits were made, FF is possible - you still want sometimes to have each commit in the mainline correspond to one feature. So you treat a feature branch with a bunch of commits as a single unit, and merge them as a single unit. It is clear from your history when you do feature branch merging with `--no-ff`.

   

3. Laquelle est la plus communément utilisée dans la gestion d’un projet collaboratif ? Pourquoi ?

   the most commun way is no -ff in cooperative work. with no fast forward we can save history. 

   

4. Fusionnez la branche `develop` dans la branche `main` en utilisant la stratégie de fusion dite "non-fast-forward".

   ```bash
   git merge origin/develop
   ```

   

5. Il y a conflit… Quelle commande puis-je faire pour annuler la fusion en cours ?

   ```bash
   git merge --abort
   ```

   

6. Corrigez le conflit de fusion en modifiant le code de manière appropriée. On suppose ici que c’est le code de la branche develop qui est celui à conserver.

   ![image-20230302152421183](C:\Users\marti\AppData\Roaming\Typora\typora-user-images\image-20230302152421183.png)

   we need to change "hello,world!\n" in the file example.c on the main branch

   then we can commit our modification & then merge the branch develop. 

   ```bash
   $ git merge develop -m "merge develop branch"
   Merge made by the 'recursive' strategy.
    example.c | 8 ++++++--
    1 file changed, 6 insertions(+), 2 deletions(-)
   
   ```

7. Faites un commit avec vos modifications.

8. Poussez votre branche sur le dépôt distant.

   ```bash
   $ git push
   Enumerating objects: 9, done.
   Counting objects: 100% (9/9), done.
   Delta compression using up to 8 threads
   Compressing objects: 100% (5/5), done.
   Writing objects: 100% (5/5), 588 bytes | 588.00 KiB/s, done.
   Total 5 (delta 2), reused 0 (delta 0), pack-reused 0
   To https://forge.univ-lyon1.fr/p2106820/tuto-git-fusion.git
      e71beb2..96991b5  main -> main
   
   ```

   


## III. Fusion de branches avec stratégie octopus (1 heure, 6 points)

Maintenant que vous avez compris comment fusionner des branches avec les stratégies de fusion standard, nous allons voir comment utiliser la stratégie octopus pour fusionner plusieurs branches en une seule opération.

1. Récupérez les dernières modifications

Comme précédemment, vous devez vous assurer que vous êtes à jour avec les dernières modifications des branches `develop` et `feature`. Pour cela, effectuez les commandes suivantes :
```
$ git switch develop
$ git pull
$ git switch feature
$ git pull
```
* Que fait la commande `git pull` ? Décrire son fonctionnement.

  ```
  git pull fetches (git fetch) the new commits and merges (git merge) these into your local branch.
  ```

* Pourquoi exécuter ces commandes ?

  To be up to date with the remote repository

2. Fusionner les branches avec octopus

Maintenant que vous êtes à jour, vous pouvez fusionner les branches avec la stratégie octopus. Cette stratégie permet de fusionner plusieurs branches en une seule opération.
```
$ git switch main
$ git merge develop feature
```
```bash
$ git merge -s octopus develop feature
error: Merge requires file-level merging
Trying really trivial in-index merge...
Nope.
Merge with strategy octopus failed.

```

3. Résoudre les conflits

Si des conflits surgissent lors de la fusion, vous devrez les résoudre en utilisant les mêmes techniques que celles décrites précédemment.
La version à conserver est celle contenant les fonctions `bar()` et `baz()`.

![image-20230302155543414](C:\Users\marti\AppData\Roaming\Typora\typora-user-images\image-20230302155543414.png)]

```bash
$ git add example.c

$ git status
On branch main
Your branch is up to date with 'origin/main'.

All conflicts fixed but you are still merging.
  (use "git commit" to conclude merge)

Changes to be committed:
        modified:   example.c

$ git commit -m "merge octopus branch feature and develop"
[main 700ce71] merge octopus branch feature and develop


```

4. Tester la compilation

Une fois que la fusion est terminée, vous devez tester que le code compile correctement.
Si le code compile correctement, vous avez terminé la partie III de l'examen. Cependant, dans ce cas précis, vous allez constater que le code ne compile pas. En effet, la fusion avec la branche feature a introduit un bug. Il va donc falloir utiliser `git bisect` pour trouver le commit responsable de l'introduction de ce bug.

There is an error in compilation

## III. Utilisation de Git bisect pour déterminer le commit introduisant le bug (1 heure, 6 points)

1. Comment peut-on afficher l’historique de la branche courante ? Qu'est-ce que l'identifiant d'un commit ? Où le trouve-t-on ?

   ```bash
   git log
   ```

   ![image-20230302160304402](C:\Users\marti\AppData\Roaming\Typora\typora-user-images\image-20230302160304402.png)

   

2. Comment peut-on visualiser les différences entre deux branches ? Entre deux commits ? Entre deux branches entre le dépôt local et le dépôt distant ?

   ```bash
   git diff main test # this command compares the difference between two branches
   git diff e71be # this command compares the difference between HEAD commit & commit with ID e71be
   git diff main origin/main # this command compares the difference between local main branch & remote main branch
   
   ```

   

3. On va utiliser `git bisect` pour déterminer le commit introduisant le bug.

4. Exécutez la commande `git bisect start`.

5. Exécutez la commande `git bisect bad HEAD` pour marquer le commit actuel comme étant mauvais.

6. Trouvez un commit connu pour être bon, par exemple le premier commit dans la branche main, et de l'exécuter en utilisant la commande `git bisect good <commit-id>`.

7. Vérifiez si le commit actuel est bon ou mauvais en compilant le code.

8. Exécutez `git bisect good` ou `git bisect bad` en fonction du résultat de l'étape précédente pour marquer le commit courant comme étant bon ou mauvais.

9. Répétez les étapes 6 et 7 jusqu'à ce que le commit introduisant le bug soit identifié (processus itératif).

   ```bash
   $ git bisect good
   9762e59aa944bdecd4091b8c6c59bb2012d8b0bb is the first bad commit
   commit 9762e59aa944bdecd4091b8c6c59bb2012d8b0bb
   Author: Sébastien GADRAT <sebastien.gadrat@cc.in2p3.fr>
   Date:   Wed Mar 1 16:24:22 2023 +0100
   
       Update example.c
   
    example.c | 2 +-
    1 file changed, 1 insertion(+), 1 deletion(-)
   
   ```

   

10. Terminez le processus de bisect en utilisant la commande `git bisect reset`.

11. Placez-vous sur le commit qui a introduit le bug en créant une nouvelle branche, appelée `bug1`, corrigez le bug sur cette nouvelle branche en suivant la proposition donnée lors de la tentative de compilation, puis effectuer une fusion sur main pour corriger ce bug.

    ```bash
    $ git checkout 9762e59aa944bdecd4091b8c6c59bb2012d8b0bb
    
    $ git switch -c bug1
    Switched to a new branch 'bug1'
    ```

    ![image-20230302163320184](C:\Users\marti\AppData\Roaming\Typora\typora-user-images\image-20230302163320184.png)

    ```bash
    TP GIT NOTER/tuto-git-fusion (main)
    $ git merge bug1 -m "fix bug1"
    Auto-merging example.c
    Merge made by the 'recursive' strategy.
     a.exe     | Bin 0 -> 40764 bytes
     example.c |   2 +-
     2 files changed, 1 insertion(+), 1 deletion(-)
     create mode 100644 a.exe
    
    ```

    

12. De nouveau sur la branche main, compiler le code. Que se passe-t-il ?

    ```bash
    #there is still a bug. the code does not compile
    ```

    

13. Répétez les étapes précédentes pour corriger ce second bug (en créant une nouvelle branche `bug2`), et obtenir un code qui compile.

    ```bash
    $ git bisect good
    9762e59aa944bdecd4091b8c6c59bb2012d8b0bb is the first bad commit
    commit 9762e59aa944bdecd4091b8c6c59bb2012d8b0bb
    Author: Sébastien GADRAT <sebastien.gadrat@cc.in2p3.fr>
    Date:   Wed Mar 1 16:24:22 2023 +0100
    
        Update example.c
    
     example.c | 2 +-
     1 file changed, 1 insertion(+), 1 deletion(-)
    
    ```

    ```bash
    PARC/TP GIT NOTER/tuto-git-fusion ((9762e59...))
    $ git switch -c bug2
    Switched to a new branch 'bug2'
    ```

    

14. Annulez ce dernier commit correcteur.

15. Nous allons voir ici une autre manière de porter le commit qui corrige le bug sur la branche main : on utilisera ici la commande `git cherry-pick` (qui agira comme un patch). Pour cela, placez-vous sur main, et, avec cette commande, importer et jouer le commit correcteur sur la branche `main`.
